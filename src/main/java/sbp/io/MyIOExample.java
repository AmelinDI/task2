package sbp.io;

public class MyIOExample
{
    /**
     * ������� ������ ������ {@link java.io.File}, ��������� ������������� � ��� �������� (���� ��� ����������).
     * ���� �������� ����������, �� ������� � ������� ����������:
     *      - ���������� ����
     *      - ������������ ����
     * ���� �������� �������� ������, �� ������� � �������:
     *      - ������
     *      - ����� ���������� ���������
     * ���������� ������������ ����� {@link java.io.File}
     * @param fileName - ��� �����
     * @return - true, ���� ���� ������� ������
     */
    public boolean workWithFile(String fileName)
    {
        /*
        ...
         */
        return false;
    }

    /**
     * ����� ������ ��������� ����� �����
     * ���������� ������������ IO ������ {@link java.io.FileInputStream} � {@link java.io.FileOutputStream}
     * @param sourceFileName - ��� ��������� �����
     * @param destinationFileName - ��� ����� �����
     * @return - true, ���� ���� ������� ����������
     */
    public boolean copyFile(String sourceFileName, String destinationFileName)
    {
        /*
        ...
         */
        return false;
    }

    /**
     * ����� ������ ��������� ����� �����
     * ���������� ������������ IO ������ {@link java.io.BufferedInputStream} � {@link java.io.BufferedOutputStream}
     * @param sourceFileName - ��� ��������� �����
     * @param destinationFileName - ��� ����� �����
     * @return - true, ���� ���� ������� ����������
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)
    {
        /*
        ...
         */
        return false;
    }

    /**
     * ����� ������ ��������� ����� �����
     * ���������� ������������ IO ������ {@link java.io.FileReader} � {@link java.io.FileWriter}
     * @param sourceFileName - ��� ��������� �����
     * @param destinationFileName - ��� ����� �����
     * @return - true, ���� ���� ������� ����������
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
    {
        /*
        ...
         */
        return false;
    }
}
